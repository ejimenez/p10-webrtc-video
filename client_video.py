import asyncio
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import CopyAndPasteSignaling, BYE
from aiortc.contrib.media import MediaBlackhole, MediaRecorder


# Esta función maneja el proceso de señalización entre el cliente y el servidor.
async def consume_signaling(pc, signaling):
    while True:
        # Espera la recepción de un objeto de señalización.
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            # Establece la descripción remota en el objeto RTCPeerConnection.
            await pc.setRemoteDescription(obj)

            # Guarda el SDP recibido del servidor en un archivo.
            with open("server_video.sdp", "w") as file:
                file.write(obj.sdp)

            if obj.type == "offer":
                # Si se recibe una oferta, crea y envía una respuesta.
                await pc.setLocalDescription(await pc.createAnswer())

                # Genera y guarda el SDP de la respuesta en un archivo.
                with open("client_video.sdp", "w") as file:
                    file.write(pc.localDescription.sdp)

                # Envía la respuesta al servidor.
                await signaling.send(pc.localDescription)

        elif obj is BYE:
            # Cierra la conexión si se recibe un mensaje BYE.
            print("Exiting")
            break


# Esta función se ejecuta en el lado del cliente y espera recibir pistas (tracks) de media.
async def run_answer(pc, signaling):
    # Conecta con el servidor para iniciar el intercambio de señalización.
    await signaling.connect()

    # Función para manejar las pistas de media entrantes.
    @pc.on("track")
    async def on_track(track):
        print(f"Receiving {track.kind}")

        if track.kind == "video":
            # Si la pista es de video, la graba en un archivo.
            recorder = MediaRecorder("output.mp4")
            recorder.addTrack(track)
            await recorder.start()

    # Espera y procesa la señalización entrante.
    await consume_signaling(pc, signaling)


# Punto de entrada principal del script.
if __name__ == "__main__":
    # Crea una instancia de la clase CopyAndPasteSignaling para la señalización.
    signaling = CopyAndPasteSignaling()

    # Crea una instancia de la clase RTCPeerConnection para la conexión WebRTC.
    pc = RTCPeerConnection()

    # Ejecuta la función run_answer en una corutina.
    coro = run_answer(pc, signaling)

    # Obtiene el bucle de eventos asyncio.
    loop = asyncio.get_event_loop()

    try:
        # Ejecuta la corutina hasta su finalización.
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        # Maneja la interrupción del teclado para cerrar adecuadamente.
        pass
    finally:
        # Asegura que todos los recursos se liberen correctamente al terminar.
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())
