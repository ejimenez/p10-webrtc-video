import asyncio
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer
from aiortc.contrib.signaling import CopyAndPasteSignaling, BYE


# Esta función maneja el proceso de señalización entre el cliente y el servidor.
async def consume_signaling(pc, signaling):
    while True:
        # Espera la recepción de un objeto de señalización.
        obj = await signaling.receive()

        if isinstance(obj, RTCSessionDescription):
            # Establece la descripción remota en el objeto RTCPeerConnection.
            await pc.setRemoteDescription(obj)

            # Guarda el SDP recibido del cliente en un archivo.
            with open("client_video.sdp", "w") as file:
                file.write(obj.sdp)

            if obj.type == "offer":
                # Si se recibe una oferta, crea y envía una respuesta.
                await pc.setLocalDescription(await pc.createAnswer())
                await signaling.send(pc.localDescription)

        elif obj is BYE:
            # Cierra la conexión si se recibe un mensaje BYE.
            print("Exiting")
            break


# Esta función inicializa la conexión WebRTC y prepara el envío de la oferta.
async def run_offer(pc, signaling):
    # Conecta con el servidor para iniciar el intercambio de señalización.
    await signaling.connect()

    # Crea un reproductor multimedia y añade un track de video al peer connection.
    player = MediaPlayer("video.webm")
    pc.addTrack(player.video)

    # Crea y envía una oferta WebRTC.
    await pc.setLocalDescription(await pc.createOffer())

    # Guarda el SDP generado en un archivo.
    with open("server_video.sdp", "w") as file:
        file.write(pc.localDescription.sdp)

    # Envía la oferta al cliente.
    await signaling.send(pc.localDescription)

    # Espera y procesa la señalización entrante.
    await consume_signaling(pc, signaling)


# Punto de entrada principal del script.
if __name__ == "__main__":
    # Crea una instancia de la clase CopyAndPasteSignaling para la señalización.
    signaling = CopyAndPasteSignaling()

    # Crea una instancia de la clase RTCPeerConnection para la conexión WebRTC.
    pc = RTCPeerConnection()

    # Ejecuta la función run_offer en una corutina.
    coro = run_offer(pc, signaling)

    # Obtiene el bucle de eventos asyncio.
    loop = asyncio.get_event_loop()

    try:
        # Ejecuta la corutina hasta su finalización.
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        # Maneja la interrupción del teclado para cerrar adecuadamente.
        pass
    finally:
        # Asegura que todos los recursos se liberen correctamente al terminar.
        loop.run_until_complete(player.stop())  # player es necesario para detenerse
        loop.run_until_complete(pc.close())
        loop.run_until_complete(signaling.close())
